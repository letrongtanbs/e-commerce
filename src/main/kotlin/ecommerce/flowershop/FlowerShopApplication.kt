package ecommerce.flowershop

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FlowerShopApplication

fun main(args: Array<String>) {
	runApplication<FlowerShopApplication>(*args){
		setBannerMode(Banner.Mode.OFF)
	}
}
